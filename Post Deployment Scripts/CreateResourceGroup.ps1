$rgExists = az group exists --name $ResourceGroupName

if($rgExists -eq $false){
    az group create --location $Location --name $ResourceGroupName --subscription $SubscriptionID
    Write-Host("$ResourceGroupName - resource group successfully created")
}
else{
    Write-Host("$ResourceGroupName - resource group exists")
}