#-------------------------------------------------------------------------------------------------
#Should be executed after SFC and Node steps is ran
#-------------------------------------------------------------------------------------------------

#Set system assigned identity to ON in VMSS
	az vmss identity assign --resource-group $ResourceGroupName --name $VMScaleSetName


#----Provide VMSS access to the keyvault secrets
	$vaultName = "kv-$GenericResourceName"
	$vmssObjectid = az vmss list --resource-group $ResourceGroupName --out "tsv" --query "[0].identity.principalId"
	az keyvault set-policy --name $vaultName --key-permissions get list update create import delete recover backup restore decrypt encrypt unwrapKey wrapKey verify sign --secret-permissions get list --object-id $vmssObjectid 


#Get Internal load balancer Front end IP address
	$internallbName = "lb-$GenericResourceName-internal"
	$internallbFrontendIP = az network lb frontend-ip list --resource-group $ResourceGroupName --lb-name $internallbName --out tsv --query "[0].privateIpAddress"

#Store the Internal Load Balancer Front end IP in Octopus variable
	Set-OctopusVariable -name "internallbFrontendIP" -value $internallbFrontendIP

#Resize OS drive Volume on all VMSS instances
	$instances = $VMInstanceCount
	for($id=0; $id -le $instances - 1; $id++) 
	{
		az vmss run-command invoke  --command-id "RunPowerShellScript" --name $VMScaleSetName --resource-group $ResourceGroupName --subscription $SubscriptionID --scripts "Get-Disk | Where partitionstyle -eq 'raw' | sort number | Initialize-Disk -PartitionStyle MBR -PassThru" --instance-id $id
		az vmss run-command invoke  --command-id "RunPowerShellScript" --name $VMScaleSetName --resource-group $ResourceGroupName --subscription $SubscriptionID --scripts "'select disk 1', 'convert dynamic', 'select disk 2', 'convert dynamic', 'select volume d', 'extend disk=2' | diskpart" --instance-id $id
	}

#Restart VMSS

	az vmss restart --name $VMScaleSetName --resource-group $ResourceGroupName