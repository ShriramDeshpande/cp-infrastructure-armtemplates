#-------------------------------------------------------------------------------------------------
#Define Variables
#-------------------------------------------------------------------------------------------------
$path = Convert-Path .
$certName = $GenericResourceName + "cert"
$filePath = "$path\$certName.pfx"
$vaultName = "kv-$GenericResourceName"
$iothName = "ioth-$GenericResourceName"
$cdbName = "cdb-$GenericResourceName"
$bsaName = "bsa$GenericResourceName"
$ssName = "ss-$GenericResourceName"
$aiName = "ai-$GenericResourceName"
$sbnName = "sbn-$GenericResourceName"
$signalrName = "sr-$GenericResourceName"
$ehsName = "ehs-$GenericResourceName"
$env = $GenericResourceName.Substring(8)
$cpappID = "5120421a-bf5d-4033-8cdd-bb6cf911ee92"

#-------------------------------------------------------------------------------------------------
# Set Environment specific variable
#-------------------------------------------------------------------------------------------------

if ($env -eq "int")
{
	$cpappID = "022c31be-059e-4712-9933-479e914ffa56"
}
elseif($env -eq "test")
{	
	$cpappID = "95d5c6a9-e76b-4844-980c-f420aaaa2ff9"
}
else
{
	#points to dev
	$cpappID = "5120421a-bf5d-4033-8cdd-bb6cf911ee92"
}


#-------------------------------------------------------------------------------------------------
# Upload certificate to KeyVault
#-------------------------------------------------------------------------------------------------

Write-Host "Certificate path is $filePath"
[System.IO.File]::WriteAllBytes($filePath, [System.Convert]::FromBase64String($OctopusParameters["Certificate.RawOriginal"]))

try{
	az keyvault certificate import --file $filePath --name $certName --vault-name $vaultName
}
catch{
    Write-Warning "Failed to upload certificate"
}

#-------------------------------------------------------------------------------------------------
#Get certificate values
#-------------------------------------------------------------------------------------------------
$keyVaultID = az keyvault show --name $vaultName --resource-group $ResourceGroupName --output "tsv" --query "id"
$certificateUrl = az keyvault certificate show --name $certName  --vault-name $vaultName --output "tsv" --query "sid" 
$thumbprint = az keyvault certificate show --name $certName --vault-name $vaultName --output "tsv" --query "x509ThumbprintHex"


#-------------------------------------------------------------------------------------------------
#Store the certificate values in octopus output variable so that it is used as input in the SFC and Node step
#-------------------------------------------------------------------------------------------------

Set-OctopusVariable -name "keyVaultID" -value $keyVaultID -sensitive
Set-OctopusVariable -name "certificateUrl" -value $certificateUrl -sensitive
Set-OctopusVariable -name "thumbprint" -value $thumbprint -sensitive


#-------------------------------------------------------------------------------------------------
#Insert secrets in keyvault
#-------------------------------------------------------------------------------------------------

$readWriteConnectionString = az iot hub policy list --subscription $SubscriptionID --resource-group $ResourceGroupName --hub-name $iothName --output "json" --out "tsv" --query "[?keyName== 'registryReadWrite'].primaryKey | [0]"
$ServiceConnectionString = az iot hub policy list --subscription $SubscriptionID --resource-group $ResourceGroupName --hub-name $iothName --output "json" --out "tsv" --query "[?keyName== 'service'].primaryKey | [0]"

$appName = $GenericResourceName + "servicebusapp"
$wsEventhubName = "eh-wetstock$env"

$keyVault =@{
				"AzureSubscriptionID" = $SubscriptionID
				"AzureTenantId" = az account show --subscription "$SubscriptionID" -o "tsv" --query "tenantId"
				"CosmosDbAuthKey" = az cosmosdb list-keys --subscription $SubscriptionID --resource-group $ResourceGroupName --name $cdbName --out "tsv" --query "primaryMasterKey"
				"CosmosDbServiceEndPoint" = az cosmosdb list --subscription $SubscriptionID --resource-group $ResourceGroupName --out json --out "tsv" --query "[0].documentEndpoint"
				"InstrumentationKey" = az resource show --subscription $SubscriptionID --resource-type "Microsoft.Insights/components" --resource-group $ResourceGroupName --name $aiName --out "tsv" --query "properties.InstrumentationKey"
				"IoTHubRegistryReadWriteConnectionString" = "HostName=$iothName.azure-devices.net;SharedAccessKeyName=registryReadWrite;SharedAccessKey=$readWriteConnectionString"
				"MaxConcurrentCalls" = "1"
				"SearchApiVersion" = "2017-11-11"
				"IoTHubServiceConnectionString" = "HostName=$iothName.azure-devices.net;SharedAccessKeyName=service;SharedAccessKey=$ServiceConnectionString"
				"SearchServiceAdminApiKey" = az search admin-key show --subscription $SubscriptionID --resource-group $ResourceGroupName --service-name $ssName --out "tsv" --query "primaryKey"
				"SearchServiceName" = $ssName
				"SearchServiceQueryApiKey" = az search query-key list --subscription $SubscriptionID --resource-group $ResourceGroupName --service-name $ssName --out "tsv" --query "[0].key"
				"SmartsyncBacklogDatabaseId" = $CosmosDBName
				"SmartsyncBacklogCollectionId" = $CosmosCollectionName
				"StorageAccountKey" = az storage account keys list --subscription $SubscriptionID --resource-group $ResourceGroupName --account-name $bsaName --out "tsv" --query "[?keyName == 'key1'].value | [0]"
				"StorageAccountName" = $bsaName
				"TableStorageConnectionString" = az storage account show-connection-string --subscription $SubscriptionID --resource-group $ResourceGroupName --name $bsaName  --key "primary" --out "tsv" --query "connectionString"
                "PublicUserMgmtCertificate" = $PublicUserMgmtCertificateValue
                "MasterDataApiAddress" = "https://api.dataservices-qa.doverfs.com:8735"
                "IoTHubHostName" = az iot hub show --resource-group $ResourceGroupName --name $iothName --out "tsv" --query "properties.hostName"
                "ServiceBusAppAdApplicationSecret" = $ServiceBusAppPassword
				"ServiceBusAppAdApplicationId" = az ad app list --display-name $appName --out "tsv" --query "[0].appId"
                "DataServicesApiAddress" = $DataServicesApiAddressValue
                "MasterDataApiPort" = $MasterDataApiPortValue
                "UserManagementApiPort" = $UserManagementApiPortValue
                "ServiceBusNamespaceRootManageSharedConnectionString" = az servicebus namespace authorization-rule keys list --resource-group $ResourceGroupName --namespace-name $sbnName --name "RootManageSharedAccessKey" --out "tsv" --query "primaryConnectionString"
				"ServiceBusNamespaceRootListenConnectionString" = az servicebus namespace authorization-rule keys list --resource-group $ResourceGroupName --namespace-name $sbnName --name "RootListenSharedAccessKey" --out "tsv" --query "primaryConnectionString"
				"ServiceBusNamespaceRootSendConnectionString" = az servicebus namespace authorization-rule keys list --resource-group $ResourceGroupName --namespace-name $sbnName --name "RootSendSharedAccessKey" --out "tsv" --query "primaryConnectionString"
				"UserManagementApiAuthenticationPort" = $UserManagementApiAuthenticationPortValue
				"UserManagementApiAuthenticationClientId" = $UserManagementApiAuthenticationClientIdValue
				"UserManagementApiAuthenticationSecret" = $UserManagementApiAuthenticationSecretValue
				"ApplicationDnsName" = $DNSName
				"AzureSignalRServiceConnectionString" = az  signalr key list --name $signalrName --resource-group $ResourceGroupName --out "tsv" --query "primaryConnectionString"
				"IotHubQueueName" = "security-token-queue"
				"DeviceConfigInputQueueName" = "device-config-queue"
				"WetStockEventHubName" = $wsEventhubName
				"WetStockEventHubSendPolicyConnectionString" = az eventhubs eventhub authorization-rule keys list --eventhub-name $wsEventhubName --resource-group $ResourceGroupName --namespace-name $ehsName --name "SendPolicy" --out "tsv" --query "primaryConnectionString"
				"WetStockEventHubListenPolicyConnectionString" = az eventhubs eventhub authorization-rule keys list --eventhub-name $wsEventhubName --resource-group $ResourceGroupName --namespace-name $ehsName --name "ListenPolicy" --out "tsv" --query "primaryConnectionString"
				"UserManagementCloudPosApplicationId" = $cpappID

}

#Create secrets
foreach($secret in $keyVault.GetEnumerator())
{
	Write-Host "$($secret.Name) : $($secret.Value)"
	az keyvault secret set --vault-name $vaultName --name $secret.Name --value $secret.Value
}

#-------------------------------------------------------------------------------------------------
#Insert Keys in keyvault
#-------------------------------------------------------------------------------------------------
az keyvault key create --vault-name $vaultName --name "BlobEncryption" --kty "RSA" --size "2048"
az keyvault key create --vault-name $vaultName --name "SecurityTokenKey" --kty "RSA" --size "2048"
az keyvault key create --vault-name $vaultName --name "InternalIssuerSigningKey" --kty "RSA" --size "2048"

#-------------------------------------------------------------------------------------------------
# Set Keyvault policies
#-------------------------------------------------------------------------------------------------

#Adding Pradeep to KeyVault
az keyvault set-policy --name $vaultName --key-permissions get list update create import delete recover backup restore --secret-permissions get list set delete recover backup restore --certificate-permissions backup create delete deleteissuers get getissuers import list listissuers managecontacts manageissuers recover restore setissuers update --object-id "e52fd5a1-aa99-4ccb-8298-cd921c5e1fd7" 

#Adding Bram to KeyVault
az keyvault set-policy --name $vaultName --key-permissions get list update create import delete recover backup restore --secret-permissions get list set delete recover backup restore --certificate-permissions backup create delete deleteissuers get getissuers import list listissuers managecontacts manageissuers recover restore setissuers update --object-id "5b22204f-1e7e-4d63-b405-ba9028299418" 

#Adding Greg to KeyVault
az keyvault set-policy --name $vaultName --key-permissions get list update create import delete recover backup restore --secret-permissions get list set delete recover backup restore --certificate-permissions backup create delete deleteissuers get getissuers import list listissuers managecontacts manageissuers recover restore setissuers update --object-id "f8f007d3-9125-4cd2-8c31-c8a130dde92b" 

