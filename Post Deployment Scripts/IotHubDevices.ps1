#-------------------------------------------------------------------------------------------------
# Pre-req - ensure iot extension is installed before running this script manually. If not intsalled run the below command
#-------------------------------------------------------------------------------------------------

#az extension add --name azure-cli-iot-ext

$env = "dev"    #probable values : dev,demo,int,test
$iothName = "ioth-cloudposint"
$resourceGroup  = "CloudPOS-Int"


if ($env -eq "int")
{
	# Integration
	az iot hub device-identity create --hub-name $iothName --device-id "ced79ebe-8540-463a-843e-45421a3a5153" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "ced79ebe-8540-463a-843e-45421a3a5153" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="eyKNost1pg4XlyhOIegQt/ecGKvep8ZfHGpmGMtd9yw="
	
	az iot hub device-identity create --hub-name $iothName --device-id "910a1e8a-a5b2-4018-be06-46d2c41d1bbc" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "910a1e8a-a5b2-4018-be06-46d2c41d1bbc" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="siunHvx5LqLpKakwRMKXyiJB5BWGS6C8YO2hEzO5vFM="
	
	az iot hub device-identity create --hub-name $iothName --device-id "03de00ca-d005-42b2-afd6-e37684aa7093" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "03de00ca-d005-42b2-afd6-e37684aa7093" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="WA39QEoyhWJERRkc2ydYCRpIMYxqhC+Eek7SgFpPxWI="
	
	az iot hub device-identity create --hub-name $iothName --device-id "c4856923-e8dd-413e-8eb8-13ccbac4fcd0" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "c4856923-e8dd-413e-8eb8-13ccbac4fcd0" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="r6h7saRiLJ8TByrKX7tvZG9xgOLiwGM4bOqVL1kgQu4="
	
	az iot hub device-identity create --hub-name $iothName --device-id "adb6dfcf-eeca-41ad-b9fd-755225e38577" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "adb6dfcf-eeca-41ad-b9fd-755225e38577" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="ruwHo4xMEktYkUXM+RPueih22ArdgLoatMrLYzre+/4="
	
	az iot hub device-identity create --hub-name $iothName --device-id "69baac5b-4143-4140-b89f-919411c33302" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "69baac5b-4143-4140-b89f-919411c33302" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="oqsMRWyN7ynavF8DJKrHKZqPAd5jjIlTayxddlvWo/8="

}
elseif($env -eq "test")
{	
	# Test
	az iot hub device-identity create --hub-name $iothName --device-id "5f3388ff-71cb-402b-af33-8ed7b1565599" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "5f3388ff-71cb-402b-af33-8ed7b1565599" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="wDFlvkVIWEj7FtSbZFx+2XSNmVlHPvud0Ss7Sl9DTFs="
	
	az iot hub device-identity create --hub-name $iothName --device-id "dc7bae3d-8895-4714-b396-914ff7611881" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "dc7bae3d-8895-4714-b396-914ff7611881" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="zPNdb19W/vpoER1oB7Mhl8SQngl63DxGR9vvX4aDt3c="
	
	az iot hub device-identity create --hub-name $iothName --device-id "250981cc-10f3-40af-afb6-64549ba2ca7a" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "250981cc-10f3-40af-afb6-64549ba2ca7a" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="mSGjBHgK7Q3fBAquG2EY+kOT9VohhC6YaXN48Enls6I="

}
else
{
	# Dev/Demo
	az iot hub device-identity create --hub-name $iothName --device-id "420e4c6e-dd52-4a5e-8453-0561870c6ca0" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "420e4c6e-dd52-4a5e-8453-0561870c6ca0" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="64coZ35as4I8zuiaIeiMP/0eLH8FvFkXeB2Cc+QahhM="
	
	az iot hub device-identity create --hub-name $iothName --device-id "070b4554-f069-4dca-94ec-9fe56ecdce12" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "070b4554-f069-4dca-94ec-9fe56ecdce12" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="C8gWaQseCQxjF4di7NaZ184cd26pBPXRxOCCoyyWl7c="
	
	az iot hub device-identity create --hub-name $iothName --device-id "607e0768-e041-44e4-95c6-459629ff0271" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "607e0768-e041-44e4-95c6-459629ff0271" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="OEYvc5gcsI0bsGFLOzJAIxuqExmy47ojcddkqfk3TuU="
	
	az iot hub device-identity create --hub-name $iothName --device-id "7dca6efa-3771-407f-92ea-dd2499fbadac" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "7dca6efa-3771-407f-92ea-dd2499fbadac" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="PWwbDJ/GbmpnU7soo8iuDoby3d8su1rrRlnK8nHKDPg="
	
	az iot hub device-identity create --hub-name $iothName --device-id "3d8b7d23-68e5-4a61-9559-8b3abde2cf73" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "3d8b7d23-68e5-4a61-9559-8b3abde2cf73" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="yHkzk/M1ZrEa3G2wXW6vtFJG21MjrA2eRGb3rZNuXzI="
	
	az iot hub device-identity create --hub-name $iothName --device-id "d59ebfa1-0a3a-4dfb-a81c-191ee85596c9" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "d59ebfa1-0a3a-4dfb-a81c-191ee85596c9" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="Y8PBVDf9rGg3+J0LbJLlvPGobzoLWFeNaU/h1pyTMNY="
	
	az iot hub device-identity create --hub-name $iothName --device-id "d1c92448-967a-4800-bd82-4fa352dc93e6" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "d1c92448-967a-4800-bd82-4fa352dc93e6" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="QYpJaEzazRe2aWZcJiPSiaYyZ9/5VUyvwxHc49RnTFQ="
	
	az iot hub device-identity create --hub-name $iothName --device-id "0eaa1c20-b467-4a73-ac98-caef57031bed" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "0eaa1c20-b467-4a73-ac98-caef57031bed" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="VTYLgqRY2POlk5YCLChiTuhUJytGLKXHIVOitEg4Lf0="
	
	az iot hub device-identity create --hub-name $iothName --device-id "9d77cb80-c914-4f54-8365-bd7f198b99e4" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "9d77cb80-c914-4f54-8365-bd7f198b99e4" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="mwVdWWc4D3NtVt+sBWv4EBSa+JbM1CtxjNcmQWb2jY0="	
	
	az iot hub device-identity create --hub-name $iothName --device-id "618a15d0-76a4-4488-b8e7-5872c3151876" --resource-group $resourceGroup 
	az iot hub device-identity update --hub-name $iothName --device-id "618a15d0-76a4-4488-b8e7-5872c3151876" --resource-group $resourceGroup --set authentication.symmetricKey.primaryKey="cRbfwUU0d/Vt8I81D3CEjYjz/pP1M3M+zeceFfox9f4="
	
}

