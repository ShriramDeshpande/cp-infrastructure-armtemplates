#-------------------------------------------------------------------------------------------------
#Define Variables
#-------------------------------------------------------------------------------------------------

	$sbnName = "sbn-$GenericResourceName"
	$servicetokenqueueName = "security-token-queue"
	$deviceconfigqueueName = "device-config-queue"
	$queueAuthRuleName = "iothubroutes_$ResourceGroupName"
	$servicetokenrouteName = "security-token-routing"
	$deviceconfigrouteName = "device-config-routing"
	$servicetokenroutingQuery = 'MessageId="GenerateSecurityToken"'
	$deviceconfigroutingQuery = 'MessageId="GetConfiguration"'
	$iothName = "ioth-$GenericResourceName"
	$cdbName = "cdb-$GenericResourceName"
	$storageAccountName = "bsa$GenericResourceName"
	$appName = $GenericResourceName + "servicebusapp"
	$appUrl = "http://" + $appName


#-------------------------------------------------------------------------------------------------
#Creates collection on Cosmos DB
#-------------------------------------------------------------------------------------------------

	az cosmosdb database create --db-name $CosmosDBName --resource-group-name $ResourceGroupName --name $cdbName
	Start-Sleep -s 10
	az cosmosdb collection create --collection-name $CosmosCollectionName --db-name $CosmosDBName --name $cdbName --resource-group-name $ResourceGroupName --partition-key-path "/Status" --throughput "400"

#-------------------------------------------------------------------------------------------------
#Updates blob's service properties on storage account
#-------------------------------------------------------------------------------------------------

	az storage blob service-properties update --account-name $storageAccountName --index-document "index.html" --static-website true

#-------------------------------------------------------------------------------------------------
# Create AZ AD App and service principal account
#-------------------------------------------------------------------------------------------------

	$appExists = az ad app list --display-name $appName --out "tsv" --query "[0].appId"
	if (!$appExists)
	{
		#create new azure ad app
		az ad app create --display-name $appName --native-app false --password $ServiceBusAppPassword --identifier-uris $appUrl --homepage $appUrl
		
		#Create service principal account for the new ad app
		$appId = az ad app list --display-name $appName --out "tsv" --query "[0].appId"
		az ad sp create --id $appId
			
	}

#-------------------------------------------------------------------------------------------------
# Create Service bus queue and add auth rule to the queue
#-------------------------------------------------------------------------------------------------
	# Adding security-token-queue
	az servicebus queue create --resource-group $ResourceGroupName --namespace-name $sbnName --name $servicetokenqueueName --subscription $SubscriptionID --default-message-time-to-live "14:00:00" --lock-duration "0:00:30" --duplicate-detection-history-time-window "0:10:00" --max-delivery-count "10" --max-size 1024
	az servicebus queue authorization-rule create --resource-group $ResourceGroupName --namespace-name $sbnName --queue-name $servicetokenqueueName --name $queueAuthRuleName --rights Send
	
	# Adding device-config-queue
	az servicebus queue create --resource-group $ResourceGroupName --namespace-name $sbnName --name $deviceconfigqueueName --subscription $SubscriptionID --default-message-time-to-live "14:00:00" --lock-duration "0:00:30" --duplicate-detection-history-time-window "0:10:00" --max-delivery-count "10" --max-size 1024
	az servicebus queue authorization-rule create --resource-group $ResourceGroupName --namespace-name $sbnName --queue-name $deviceconfigqueueName --name $queueAuthRuleName --rights Send
	
#-------------------------------------------------------------------------------------------------
# Create Iot Hub endpoint and asscoiate with the route
#-------------------------------------------------------------------------------------------------
	
	# Adding security-token-routing
	$connString = az servicebus queue authorization-rule keys list --resource-group $ResourceGroupName  --namespace-name $sbnName --queue-name $servicetokenqueueName --name $queueAuthRuleName --query "primaryConnectionString" --output tsv
	az iot hub routing-endpoint create --resource-group $ResourceGroupName --connection-string $connString --hub-name $iothName --endpoint-name $servicetokenqueueName --endpoint-type servicebusqueue --endpoint-resource-group $ResourceGroupName --endpoint-subscription-id $SubscriptionID
	az iot hub route create --resource-group $ResourceGroupName --hub-name $iothName --endpoint-name $servicetokenqueueName --source-type DeviceMessages --route-name $servicetokenrouteName --condition $servicetokenroutingQuery --enabled true  --subscription $SubscriptionID

	# Adding device-config-queue
	$connString = az servicebus queue authorization-rule keys list --resource-group $ResourceGroupName  --namespace-name $sbnName --queue-name $deviceconfigqueueName --name $queueAuthRuleName --query "primaryConnectionString" --output tsv
	az iot hub routing-endpoint create --resource-group $ResourceGroupName --connection-string $connString --hub-name $iothName --endpoint-name $deviceconfigqueueName --endpoint-type servicebusqueue --endpoint-resource-group $ResourceGroupName --endpoint-subscription-id $SubscriptionID
	az iot hub route create --resource-group $ResourceGroupName --hub-name $iothName --endpoint-name $deviceconfigqueueName --source-type DeviceMessages --route-name $deviceconfigrouteName --condition $deviceconfigroutingQuery --enabled true  --subscription $SubscriptionID









