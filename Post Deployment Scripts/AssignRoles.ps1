#-------------RUN THIS SCRIPT MANUALLY AT THE END----------------------
#-------------Requires Owner access to run this script-----------------

$ResourceGroupName = "CloudPOS-Test"
$GenericResourceName = "cloudpostest"
$appName = $GenericResourceName + "servicebusapp"
$SubscriptionID = "d3e9bf31-eba7-4fd9-946b-f812b2868b05"

#---------Contributor--------------------
  
az role assignment create --assignee bram.desmedt@doverfs.com --resource-group $ResourceGroupName  --role Contributor
az role assignment create --assignee haider.ali@doverfs.com --resource-group $ResourceGroupName  --role Contributor
az role assignment create --assignee dries.dehoon@doverfs.com --resource-group $ResourceGroupName  --role Contributor
az role assignment create --assignee sven.janssen@doverfs.com --resource-group $ResourceGroupName  --role Contributor
az role assignment create --assignee grzegorz.wroblewski@doverfs.com --resource-group $ResourceGroupName  --role Contributor
az role assignment create --assignee conrad.vanvugt@doverfs.com --resource-group $ResourceGroupName  --role Contributor 

#---------Developer--------------------

az role assignment create --assignee artyom.avetisyan@doverfs.com --resource-group $ResourceGroupName  --role Developer
az role assignment create --assignee midas.lambrichts@doverfs.com --resource-group $ResourceGroupName  --role Developer
az role assignment create --assignee raymond.leijtens@doverfs.com --resource-group $ResourceGroupName  --role Developer
az role assignment create --assignee natalia.skwirut@doverfs.com --resource-group $ResourceGroupName  --role Developer
az role assignment create --assignee rafal.wojcik@doverfs.com --resource-group $ResourceGroupName  --role Developer

#---------Cost Management Reader--------------------

az role assignment create --assignee annick.zegers@doverfs.com --resource-group $ResourceGroupName  --role "Cost Management Reader"


#---Add service bus app as contributor to resource servicebus namespace
$appScope = "/subscriptions/$SubscriptionID/resourceGroups/$ResourceGroupName/providers/Microsoft.ServiceBus/namespaces/sbn-$GenericResourceName/"

#Get the Service principal Object Id associated with the service bus app
$servicePrincipalObjectId = az ad sp list --display-name $appName --out "tsv" --query "[0].objectId"

az role assignment create --role "Contributor" --assignee-object-id $servicePrincipalObjectId --scope $appScope --subscription  $SubscriptionID