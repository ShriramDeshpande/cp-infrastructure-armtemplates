# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repo includes the ClousPOS Infrastructure Code (ARM Templates) along with Post deployment scripts
* 1.0.0

### How do I get set up? ###

* This is maintained/executed ONLY by DevOps
* Run the build configuration, this should deploy the zipped package on to the Octopus Deploy server
* Create a manual release from Octopus and select/ensure the latest deployed package is picked
* select the environment to be created
* Click deploy

### Contribution guidelines ###

* The Dev team is allowed to manage the key vault post deployment script , apart from that everything else is maintained by DevOps

### Who do I talk to? ###

* Repo owner - pradeep.ramaprasad@doverfs.com
* Please do not provide any access to this repo without the reop owner's approval